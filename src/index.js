import  React from 'react';
import { render } from 'react-dom';
import Blog from './components/Blog';
import AuthForm from './components/AuthForm';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
  } from "react-router-dom";

import './index.css';

render(
    <div>
        <h2 className="title-text">My Blogs From React</h2>
        <Router>
            <div>
                {/* <ul>
                    <li> */}
                        <Link to="/">Home</Link>
                    {/* </li>
{/*                     
                    <li>
                        <Link to="/login">Authentication</Link>
                    </li> */}
                {/* </ul> */}

                <hr />

                <Switch>
                <Route exact path="/">
                    <Blog />
                </Route>
                <Route path="/login">
                    <AuthForm />
                </Route>
                </Switch>
            </div>
        </Router>
    </div>,

    document.getElementById('root')
);