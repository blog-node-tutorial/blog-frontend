import React, { Component } from 'react';
import image from '../assets/blog.png'

class BlogItem extends Component {

    state = {
        id: this.props.blog.id,
        title: this.props.blog.title,
        body: this.props.blog.body,
        author: this.props.blog.username
    }
    
    render() {
        return (
            <div>
                <img src={image} className='blog-image'></img>
                {this.state.id}. <h3 className="blog-title">{this.state.title}</h3>
                <p className="blog-body">{this.state.body}</p>
                <br />  
                Author : {this.state.author}
                <hr /> 
            </div>
        )
    }
}

export default BlogItem;