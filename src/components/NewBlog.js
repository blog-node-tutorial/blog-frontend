import React, { Component } from 'react';
import { Button, InputGroup, FormControl } from 'react-bootstrap';

class Blog extends Component {

    state = {
        title:'',
        body:''
    }


    updateTitle = () => {
        this.setState({ title: event.target.value });
    }
    updateBody = () => {
        this.setState({ body: event.target.value });
    }

    submit = () => {

        const { title, body } = this.state;
        console.log(JSON.stringify({title, body }))
                
        fetch('http://localhost:4000/blog/new',{
            method: 'POST',
            body: JSON.stringify({ title, body }),
            headers: {'Content-Type': 'application/json'},
            credentials: 'include'
        })
        .then(response => response.json())
        .then(json => {
            console.log(JSON.stringify(json));
            this.setState({ redirect: true });
        })
        .catch(error => console.log(error));
    }



    render(){
        return(

            <div>
                <h2>New Blog</h2>
                <form>
                    <label>
                        title
                        <FormControl 
                            type="text" 
                            value={this.state.title} 
                            onChange={this.updateTitle} />
                    </label>
                    <br/>
                    <label>
                        body
                        <FormControl 
                            type="text" 
                            value={this.state.body} 
                            onChange={this.updateBody} />
                    </label>
                    <br/>

                    <Button onClick={this.submit}>Submit</Button>

                </form>
            </div>
        )
    }

}
export default Blog;