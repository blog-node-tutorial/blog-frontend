import React, { Component } from 'react';
import { Button, FormGroup, FormControl } from 'react-bootstrap'
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
  } from "react-router-dom";

import NewBlog from './NewBlog'

  
class AuthForm extends Component{

    state = {
        username : '',
        password : '',
        redirect: false
    }

    updateUsername = () => {
        this.setState({ username: event.target.value})
    }

    updatePassword = () => {
        this.setState ({password: event.target.value});
    }

    login = () => {

        const { username, password } = this.state;
        console.log('username ', username);
        console.log(JSON.stringify({username, password }))
                
        fetch('http://localhost:4000/account/login',{
            method: 'POST',
            body: JSON.stringify({ username, password }),
            headers: {'Content-Type': 'application/json'},
            credentials: 'include'
        })
        .then(response => response.json())
        .then(json => {
            console.log(JSON.stringify(json));
            this.setState({ redirect: true });
        })
        .catch(error => console.log(error));
    }

    checkLogin = () => {
                
        fetch('http://localhost:4000/account/authenticated',{
            credentials: 'include'
        })
        .then(response => response.json())
        .then(json => {
            console.log(JSON.stringify(json));
        })
        .catch(error => console.log(error));
    }

    logout = () => {
                
        fetch('http://localhost:4000/account/logout',{
            credentials: 'include'
        })
        .then(response => response.json())
        .then(json => {
            console.log(JSON.stringify(json));
        })
        .catch(error => console.log(error));
    }


    render(){
        
        const {redirect} = this.state;
        if(redirect){
            return(
                <NewBlog />
            )
        }

        return(
            <div>
                <FormGroup>
                    <FormControl
                        type='text'
                        value={this.state.username}
                        placeholder='username'
                        onChange={this.updateUsername}
                    />
                </FormGroup>

                <FormGroup>
                    <FormControl
                        type='password'
                        value={this.state.password}
                        placeholder='password'
                        onChange={this.updatePassword}
                    />
                </FormGroup>

                <Button onClick={this.login}>Sign In</Button>
                <br/>                <br/>
                <Button onClick={this.logout}>Log Out</Button>
                <br/>                <br/>
                <Button onClick={this.checkLogin}>Check Login</Button>
            </div>
        )
    }
}

export default AuthForm;