import React, { Component } from 'react';
import BlogRow from './BlogRow';

class Blog extends Component {

    state = { blogs : []}   

    componentDidMount(){
        this.fetchBlog();
    }

    fetchBlog = () => {
        fetch('http://localhost:4000/blog/home')
            .then(response => response.json())
            .then(response => {
                const blogs = response.blogs;
                this.setState({blogs});
            });
    }

    render(){
        const blogs = this.state.blogs;

        return(
            <div className="blog-list">
                {
                    blogs.map(blog => {
                        return(
                            <div key={blog.blogid}>
                                <BlogRow blog={blog}/>
                                <hr />
                            </div>
                        )
                    })
                }
            </div>
        )
    }
}

export default Blog