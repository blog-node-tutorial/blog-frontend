import React, { Component } from 'react';
import image from '../assets/blog.png'
import BlogItem from './BlogItem';

class BlogRow extends Component {

    state = {
        id: this.props.blog.id,
        title: this.props.blog.title,
        body: this.props.blog.body,
        author: this.props.blog.username,
        redirect: false
    }

    blogClicked = () => {
        this.setState({ redirect: true });
    }
    


    render(){
        
        const blog = this.props.blog;
        const {redirect} = this.state;
        if(redirect){
            return(
                <BlogItem blog={blog} />
            )
        }

        return(
            <div onClick={this.blogClicked}>
                <img src={image} className='blog-image'></img>
                    {this.props.blog.id}. <h3 className="blog-title">{this.props.blog.title}</h3>
                    <br />  
                    Author : {this.props.blog.username}
                    <hr /> 
               
            </div>
        )
    }
}

export default BlogRow;